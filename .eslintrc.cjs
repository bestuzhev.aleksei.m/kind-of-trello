module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: ["plugin:react/recommended", "plugin:import/recommended", "plugin:prettier/recommended"],
  ignorePatterns: ["dist", ".eslintrc.cjs", "vite.config.ts"],
  parser: "@typescript-eslint/parser",
  plugins: ["react", "import", "prettier", "react-refresh"],
  rules: {
    "import/order": [
      "error",
      {
        "newlines-between": "always",
        pathGroupsExcludedImportTypes: ["react"],
        alphabetize: {
          order: "asc",
          caseInsensitive: true,
        },
        groups: ["builtin", "external", "parent", "sibling", "index"],
        pathGroups: [
          {
            pattern: "react",
            group: "external",
            position: "before",
          },
        ],
      },
    ],
    "import/prefer-default-export": "off",
    "import/newline-after-import": ["error", { count: 1 }],
    "import/no-unresolved": "off",
    "no-var": "error",
    "object-curly-newline": [
      "error",
      {
        ObjectExpression: { multiline: true, minProperties: 5 },
        ObjectPattern: { multiline: true },
        ImportDeclaration: { multiline: true, minProperties: 7 },
        ExportDeclaration: { multiline: true, minProperties: 7 },
      },
    ],
    "padding-line-between-statements": [
      "error",
      { blankLine: "always", prev: "*", next: "return" },
      { blankLine: "always", prev: ["const", "let", "var"], next: "*" },
      {
        blankLine: "any",
        prev: ["const", "let", "var"],
        next: ["const", "let", "var"],
      },
    ],
    "prettier/prettier": ["error", { semi: false }],
    "react/display-name": "off",
    "react/function-component-definition": [
      2,
      {
        namedComponents: "arrow-function",
        unnamedComponents: "arrow-function",
      },
    ],
    "react/jsx-wrap-multilines": "error",
    "react/react-in-jsx-scope": "off",
    "react-refresh/only-export-components": ["warn", { allowConstantExport: true }],
    semi: ["error", "never"],
    "@typescript-eslint/no-non-null-assertion": "off",
  },
}
