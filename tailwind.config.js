/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      transitionDuration: { DEFAULT: "300ms" },
      colors: {
        "brown-900": "hsl(23,19.6%,27.4%)",
        "brown-700": "hsl(22,20%,32%)",
      },
    },
  },
  plugins: [],
}
