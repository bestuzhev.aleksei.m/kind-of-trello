import { createSlice, type PayloadAction } from "@reduxjs/toolkit"

import { ColumnData } from "./types.ts"

const initialState: ColumnData[] = [
  { id: "init-backlog", title: "Backlog", order: 0 },
  { id: "init-in-progress", title: "In Progress", order: 1 },
  { id: "init-done", title: "Done", order: 2 },
]

const slice = createSlice({
  name: "columns",
  initialState,
  reducers: {
    addColumn: (state, action: PayloadAction<{ id: string; title: string }>) => {
      state.push({ id: action.payload.id, title: action.payload.title, order: state.length })
    },
    editColumn: (state, action: PayloadAction<{ id: string; title: string }>) => {
      const column = state.find((column) => column.id === action.payload.id)

      if (column) {
        column.title = action.payload.title
      }
    },
    deleteColumn: (state, action: PayloadAction<string>) => {
      return state.filter((column) => column.id !== action.payload)
    },
  },
  selectors: { selectColumns: (state) => state },
})

export const columnsReducer = slice.reducer

export const { selectColumns } = slice.selectors

export const { addColumn, editColumn, deleteColumn } = slice.actions
