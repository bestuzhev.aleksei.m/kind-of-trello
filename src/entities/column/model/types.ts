export type ColumnData = {
  id: string
  title: string
  order: number
}
