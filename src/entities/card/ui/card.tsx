import { DragEvent, PropsWithChildren } from "react"

import { replaceCard, startMovingCard } from "../model/card.slice.ts"
import type { CardData } from "../model/types.ts"

import { useAppDispatch } from "@/app"

type Props = CardData & PropsWithChildren

export const Card = ({ children, ...cardData }: Props) => {
  const { id, title } = cardData

  const dispatch = useAppDispatch()

  const dragStartHandler = () => {
    dispatch(startMovingCard(cardData))
  }

  const dragLeaveHandler = (e: DragEvent<HTMLDivElement>) => {
    e.currentTarget.style.borderBottom = "4px solid transparent"
  }

  const dragOverHandler = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault()
    e.currentTarget.style.borderBottom = "4px solid rgb(59, 130, 246)"
  }

  const dropHandler = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault()
    dispatch(replaceCard(cardData))
    e.currentTarget.style.borderBottom = "4px solid transparent"
  }

  const dragEndHandler = (e: DragEvent<HTMLDivElement>) => {
    e.currentTarget.style.borderBottom = "4px solid transparent"
  }

  return (
    <div
      key={id}
      draggable
      onDragStart={dragStartHandler}
      onDragLeave={dragLeaveHandler}
      onDragOver={dragOverHandler}
      onDrop={dropHandler}
      onDragEnd={dragEndHandler}
      className="group w-full shadow-md bg-gray-100 rounded-lg p-2 text-gray-700 text-sm cursor-pointer
      flex justify-between items-center hover:scale-[1.02] transition-all border-b-4 border-transparent"
    >
      {title}
      {children}
    </div>
  )
}
