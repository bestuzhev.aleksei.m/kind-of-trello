import { createSlice, type PayloadAction } from "@reduxjs/toolkit"
import { v1 } from "uuid"

import { CardData, CardsState } from "./types.ts"

const initialState: CardsState = {
  data: {
    "init-backlog": [
      { id: v1(), title: "Google Auth", order: 0, columnId: "init-backlog" },
      { id: v1(), title: "WS support chat", order: 1, columnId: "init-backlog" },
      { id: v1(), title: "Subscriptions", order: 2, columnId: "init-backlog" },
    ],
    "init-in-progress": [
      { id: v1(), title: "Profile", order: 0, columnId: "init-in-progress" },
      { id: v1(), title: "Friends ", order: 1, columnId: "init-in-progress" },
    ],
    "init-done": [],
  },
  movingCard: null,
}

const slice = createSlice({
  name: "cards",
  initialState,
  reducers: {
    createCardStorage: (state, action: PayloadAction<string>) => {
      state.data[action.payload] = []
    },
    deleteCardStorage: (state, action: PayloadAction<string>) => {
      delete state.data[action.payload]
    },
    addCard: (state, action: PayloadAction<Omit<CardData, "order">>) => {
      state.data[action.payload.columnId].push({
        id: action.payload.id,
        title: action.payload.title,
        order: state.data[action.payload.columnId].length,
        columnId: action.payload.columnId,
      })
    },
    editCard: (state, action: PayloadAction<Omit<CardData, "order">>) => {
      const card = state.data[action.payload.columnId].find((card) => card.id === action.payload.id)

      if (card) {
        card.title = action.payload.title
      }
    },
    deleteCard: (state, action: PayloadAction<Pick<CardData, "id" | "columnId">>) => {
      state.data[action.payload.columnId] = state.data[action.payload.columnId].filter(
        (card) => card.id !== action.payload.id,
      )
      state.data[action.payload.columnId] = state.data[action.payload.columnId].map((card, index) => ({
        ...card,
        order: index,
      }))
    },

    startMovingCard: (state, action: PayloadAction<CardData>) => {
      state.movingCard = action.payload
    },
    replaceCard: (state, action: PayloadAction<Partial<CardData>>) => {
      if (!state.movingCard) return
      const { columnId: movingCardColumnId, order: movingOrder } = state.movingCard
      const { columnId: targetColumnId, order: targetOrder } = action.payload

      const movingCard = state.data[movingCardColumnId][movingOrder]

      state.data[movingCardColumnId].splice(movingOrder, 1)
      state.data[movingCardColumnId] = state.data[movingCardColumnId].map((card, index) => ({
        ...card,
        order: index,
      }))

      if (targetColumnId) {
        if (movingCardColumnId !== targetColumnId) {
          movingCard.columnId = targetColumnId
        }

        const insertIndex = targetOrder !== undefined ? targetOrder : state.data[targetColumnId].length

        state.data[targetColumnId].splice(insertIndex, 0, movingCard)
        state.data[targetColumnId] = state.data[targetColumnId].map((card, index) => ({
          ...card,
          order: index,
        }))
      }

      state.movingCard = null
    },
  },
  selectors: { selectCards: (state) => state.data },
})

export const cardsReducer = slice.reducer

export const { selectCards } = slice.selectors

export const {
  createCardStorage,
  addCard,
  editCard,
  deleteCard,
  deleteCardStorage,
  startMovingCard,
  replaceCard,
} = slice.actions
