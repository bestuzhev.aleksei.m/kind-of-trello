export type CardData = {
  id: string
  title: string
  order: number
  columnId: string
}

export type CardsState = {
  data: Record<string, CardData[]>
  movingCard: CardData | null
}
