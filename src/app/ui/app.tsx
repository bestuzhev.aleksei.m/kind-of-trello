import { v1 } from "uuid"

import { Layout } from "./layout.tsx"

import { useAppDispatch, useAppSelector } from "@/app"
import { createCardStorage } from "@/entities/card"
import { addColumn, selectColumns } from "@/entities/column"
import { EntityControl } from "@/features/entity-control"
import { PlusIcon } from "@/shared/assets/icons"
import { Button } from "@/shared/ui"
import { Column } from "@/widgets/column"

import "../styles/index.css"

export const App = () => {
  const columns = useAppSelector(selectColumns)

  const dispatch = useAppDispatch()

  const createColumn = (data: { title: string }) => {
    const id = v1()

    dispatch(addColumn({ id, title: data.title.trim() }))
    dispatch(createCardStorage(id))
  }

  return (
    <Layout>
      {columns?.map((column) => <Column key={column.id} {...column} />)}
      <EntityControl
        onSubmit={createColumn}
        title="Добавить колонку"
        description="Введите название для новой колонки задач"
        trigger={
          <Button
            variant="outlined"
            size="lg"
            contentAlign="space-between"
            className="basis-72 grow-0 shrink-0"
          >
            {"Добавить колонку"}
            <PlusIcon width={20} height={20} />
          </Button>
        }
      />
    </Layout>
  )
}
