import { PropsWithChildren } from "react"

import { Header } from "@/widgets/header"

export const Layout = ({ children }: PropsWithChildren) => {
  return (
    <div className="flex flex-col min-h-screen">
      <Header />
      <div
        className="grow px-5 pt-14 pb-10 bg-[url('/src/shared/assets/images/trello-bg-1.jpg')]
        bg-cover overflow-x-auto flex gap-x-5 items-start"
      >
        {children}
      </div>
    </div>
  )
}
