import { configureStore } from "@reduxjs/toolkit"

import { cardsReducer } from "@/entities/card"
import { columnsReducer } from "@/entities/column"

export const store = configureStore({
  reducer: {
    columns: columnsReducer,
    cards: cardsReducer,
  },
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
