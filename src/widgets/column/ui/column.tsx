import { DragEvent } from "react"

import { v1 } from "uuid"

import { CardList } from "./card-list.tsx"

import { useAppDispatch } from "@/app"
import { addCard, deleteCardStorage, replaceCard } from "@/entities/card"
import { type ColumnData, deleteColumn, editColumn } from "@/entities/column"
import { EntityControl } from "@/features/entity-control"
import { EntityDelete } from "@/features/entity-delete"
import { DeleteIcon, EditIcon, PlusIcon } from "@/shared/assets/icons"
import { Button } from "@/shared/ui"

export const Column = ({ id, title }: ColumnData) => {
  const dispatch = useAppDispatch()

  const createCard = (data: { title: string }) => {
    dispatch(addCard({ id: v1(), title: data.title, columnId: id }))
  }

  const editColumnTitle = (data: { title: string }) => {
    dispatch(editColumn({ id, title: data.title }))
  }

  const removeColumn = () => {
    dispatch(deleteColumn(id))
    dispatch(deleteCardStorage(id))
  }

  const dragLeaveHandler = (e: DragEvent<HTMLDivElement>) => {
    e.currentTarget.classList.remove("bg-gray-300")
  }

  const dragOverHandler = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault()
    e.currentTarget.classList.add("bg-gray-300")
  }

  const dropHandler = (e: DragEvent<HTMLDivElement>) => {
    e.preventDefault()
    e.currentTarget.classList.remove("bg-gray-300")
    dispatch(replaceCard({ columnId: id }))
  }

  const dragEndHandler = (e: any) => {
    e.currentTarget.classList.remove("bg-gray-300")
  }

  return (
    <div
      data-entity="column"
      onDragLeave={dragLeaveHandler}
      onDragOver={dragOverHandler}
      onDrop={dropHandler}
      onDragEnd={dragEndHandler}
      className="basis-72 grow-0 shrink-0 shadow-md bg-gray-200 rounded-2xl p-2 flex flex-col gap-y-2 border border-gray-400"
    >
      <div className="flex justify-between items-center">
        <h3 className="text-gray-900 font-semibold pl-2">{title}</h3>
        <div className="flex gap-1">
          <EntityControl
            onSubmit={editColumnTitle}
            title={title}
            description="Измените название колонки задач"
            defaultValues={{ title }}
            trigger={
              <Button variant="secondary" size="sm" rounded>
                <EditIcon width={20} height={20} />
              </Button>
            }
          />
          <EntityDelete
            title={`Удалить "${title}"`}
            onConfirm={removeColumn}
            trigger={
              <Button variant="secondary" size="sm" rounded>
                <DeleteIcon width={20} height={20} />
              </Button>
            }
          />
        </div>
      </div>
      <CardList columnId={id} />
      <EntityControl
        onSubmit={createCard}
        title="Добавить карточку"
        description="Введите название новой карточки задачи"
        trigger={
          <Button variant="secondary" size="sm" contentAlign="space-between">
            {"Добавить карточку"} <PlusIcon width={20} height={20} />
          </Button>
        }
      />
    </div>
  )
}
