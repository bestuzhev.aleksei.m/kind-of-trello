import { useAppDispatch, useAppSelector } from "@/app"
import { Card, selectCards, editCard, deleteCard } from "@/entities/card"
import { EntityControl } from "@/features/entity-control"
import { EntityDelete } from "@/features/entity-delete"
import { DeleteIcon, EditIcon } from "@/shared/assets/icons"
import { Button } from "@/shared/ui"

type Props = {
  columnId: string
}
export const CardList = ({ columnId }: Props) => {
  const cards = useAppSelector(selectCards)[columnId]

  const dispatch = useAppDispatch()

  return cards?.map((card) => {
    const editCardTitle = (data: { title: string }) => {
      dispatch(editCard({ id: card.id, title: data.title, columnId }))
    }

    const removeCard = () => {
      dispatch(deleteCard({ id: card.id, columnId }))
    }

    return (
      <Card key={card.id} {...card}>
        <div className="flex gap-1 transition-opacity opacity-0 focus-within:opacity-100 group-hover:opacity-100">
          <EntityControl
            onSubmit={editCardTitle}
            title={card.title}
            description="Измените название карточки задачи"
            defaultValues={{ title: card.title }}
            trigger={
              <Button variant="secondary" size="sm" rounded>
                <EditIcon width={20} height={20} />
              </Button>
            }
          />
          <EntityDelete
            title={`Удалить "${card.title}"`}
            onConfirm={removeCard}
            trigger={
              <Button variant="secondary" size="sm" rounded>
                <DeleteIcon width={20} height={20} />
              </Button>
            }
          />
        </div>
      </Card>
    )
  })
}
