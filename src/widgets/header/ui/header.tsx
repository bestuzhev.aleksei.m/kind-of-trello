import avatar from "@/shared/assets/images/avatar.png"

export const Header = () => {
  return (
    <header className="w-full px-5 h-12 flex justify-between items-center bg-brown-900">
      <h1 className="text-white font-bold text-xl">Kind of Trello</h1>
      <img src={avatar} alt="User avatar" className="w-6 h-6 rounded-full" />
    </header>
  )
}
