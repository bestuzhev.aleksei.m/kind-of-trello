import { cloneElement, ReactElement, ReactNode, useState } from "react"

import { useForm } from "react-hook-form"

import { Button, Input, Modal } from "@/shared/ui"

type FormData = {
  title: string
}

type Props = {
  onSubmit: (data: FormData) => void
  defaultValues?: FormData
  title: string
  description: string
  trigger: ReactNode
}

export const EntityControl = ({ onSubmit, defaultValues, title, description, trigger }: Props) => {
  const {
    reset,
    register,
    handleSubmit,
    formState: { errors, isDirty },
  } = useForm<FormData>({ defaultValues: defaultValues ?? { title: "" } })

  const [open, setOpen] = useState(false)

  const closeHandler = () => {
    setOpen(false)
    reset(defaultValues)
  }

  const submitHandler = (data: FormData) => {
    if (isDirty) {
      const trimmedData = { title: data.title.trim() }

      onSubmit(trimmedData)
      if (defaultValues) {
        reset(trimmedData)
      } else {
        reset({ title: "" })
      }
    }

    setOpen(false)
  }

  const triggerElement = cloneElement(trigger as ReactElement, { onClick: () => setOpen(true) })

  return (
    <>
      {triggerElement}
      <Modal title={title} open={open} onClose={closeHandler}>
        <p className="mb-4 text-md text-gray-900">{description}</p>
        <form className="flex flex-col gap-y-5" onSubmit={handleSubmit(submitHandler)}>
          <Input
            label="Название"
            error={errors.title?.message}
            {...register("title", {
              required: "Это поле обязательно",
              minLength: {
                value: 3,
                message: "Минимальная длина - 3 символа",
              },
              maxLength: {
                value: 20,
                message: "Максимальная длина - 20 символов",
              },
            })}
          />
          <Button className="self-end">Сохранить</Button>
        </form>
      </Modal>
    </>
  )
}
