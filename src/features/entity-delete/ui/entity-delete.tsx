import { cloneElement, ReactElement, ReactNode, useState } from "react"

import { Dialog } from "@/shared/ui"

type Props = {
  title: string
  trigger: ReactNode
  onConfirm: () => void
}
export const EntityDelete = ({ title, trigger, onConfirm }: Props) => {
  const [open, setOpen] = useState(false)

  const triggerElement = cloneElement(trigger as ReactElement, { onClick: () => setOpen(true) })

  const confirmHandler = () => {
    onConfirm()
    setOpen(false)
  }

  return (
    <>
      {triggerElement}
      <Dialog
        title={title}
        description='Нажмите "Подтвердить" для удаления'
        open={open}
        onClose={() => setOpen(false)}
        onConfirm={confirmHandler}
      />
    </>
  )
}
