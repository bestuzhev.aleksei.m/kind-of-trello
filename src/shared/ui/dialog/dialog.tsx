import { Button, Modal } from "@/shared/ui"

type Props = {
  title: string
  description: string
  open: boolean
  onClose: () => void
  onConfirm: () => void
}

export const Dialog = ({ title, description, open, onClose, onConfirm }: Props) => {
  return (
    <Modal title={title} open={open} onClose={onClose}>
      <p className="mb-6 text-md text-gray-900">{description}</p>
      <div className="flex justify-between">
        <Button variant="secondary" onClick={onClose}>
          Отменить
        </Button>
        <Button onClick={onConfirm}>Подтвердить</Button>
      </div>
    </Modal>
  )
}
