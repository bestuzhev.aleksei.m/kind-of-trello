import { PropsWithChildren } from "react"

import { Root, Portal, Overlay, Content, Title } from "@radix-ui/react-dialog"

import { CloseIcon } from "@/shared/assets/icons"
import { Button } from "@/shared/ui"

type Props = {
  title: string
  open: boolean
  onClose: () => void
} & PropsWithChildren

export const Modal = ({ title, open, onClose, children }: Props) => {
  return (
    <Root open={open}>
      <Portal>
        <Overlay className="fixed inset-0 z-20 bg-black/50" />
        <Content
          forceMount
          className="fixed z-50 w-[95vw] max-w-md p-6 md:w-full top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 bg-gray-200 rounded-2xl"
        >
          <Title className="text-дп font-medium text-gray-900 mb-8">{title}</Title>
          {children}
          <Button onClick={onClose} variant="secondary" className="absolute top-3.5 right-3.5" rounded>
            <CloseIcon width={24} height={24} />
          </Button>
        </Content>
      </Portal>
    </Root>
  )
}
