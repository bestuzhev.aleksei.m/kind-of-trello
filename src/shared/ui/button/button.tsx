import { ComponentPropsWithoutRef, ElementRef, forwardRef } from "react"

import clsx from "clsx"

import styles from "./button.module.css"

type Props = {
  variant?: "primary" | "secondary" | "outlined"
  contentAlign?: "center" | "space-between"
  size?: "lg" | "md" | "sm"
  fullWidth?: boolean
  rounded?: boolean
} & ComponentPropsWithoutRef<"button">

export const Button = forwardRef<ElementRef<"button">, Props>((props, ref) => {
  const {
    variant = "primary",
    size = "md",
    fullWidth = false,
    contentAlign = "center",
    rounded = false,
    className,
    ...restProps
  } = props

  const classes = clsx(
    styles.button,
    styles[variant],
    styles[size],
    styles[contentAlign],
    fullWidth && styles.fullWidth,
    rounded && styles.rounded,
    className,
  )

  return <button ref={ref} className={classes} {...restProps} />
})
