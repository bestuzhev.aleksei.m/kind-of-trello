import { ComponentPropsWithoutRef, ElementRef, forwardRef } from "react"

import clsx from "clsx"

import styles from "./input.module.css"

type Props = {
  label: string
  error?: string
  size?: "lg" | "md" | "sm"
} & Omit<ComponentPropsWithoutRef<"input">, "size">

export const Input = forwardRef<ElementRef<"input">, Props>((props, ref) => {
  const { label, error, size = "md", className, ...restProps } = props

  const classes = {
    root: clsx(styles.root, className),
    label: clsx(styles.label, error && styles.error),
    input: clsx(styles.input, styles[size]),
  }

  return (
    <div className={classes.root}>
      <label className={classes.label}>{error || label}</label>
      <input ref={ref} className={classes.input} {...restProps} />
    </div>
  )
})
