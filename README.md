# React + TypeScript + Vite

>Fast template for React SPA with linters and husky

## Installed libraries:

- **clsx**
- **eslint**
- **husky**
- **prettier**
- **react**
- **react-redux**
- **redux**
- **tailwind css**
- **typescript**
- **uuid**
- **vite**

<hr>

Тестовое задание - React , Typescript:

- создать React приложение на typescript в формате trello (todo list с возможностью передвижение карточек)

- должно быть реализовано возможность создавать колонки с карточками, передвигать карточки между колонок при помощи drag and drop, редактирование название карточки и название  колонки, создание карточек и колонок вывести в модальное окно

- дизайн свободный, использовать tailwind css

- state manager redux-toolkit, backend не нужен
- использовать yarn
